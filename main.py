import pygame, os, random, sys, neat, math
from LargeObstacle import *
from SmallObstacle import *
from Dino import *


pygame.init()

max_score = 0
SCREEN_HEIGHT = 600
SCREEN_WIDTH = 1100
SCREEN = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
BG = pygame.image.load(os.path.join("assets", "Other", "Track.png"))
SMALL_OBST = [
    pygame.image.load(os.path.join("assets", "Cactus", "SmallCactus1.png")),
    pygame.image.load(os.path.join("assets", "Cactus", "SmallCactus2.png")),
    pygame.image.load(os.path.join("assets", "Cactus", "SmallCactus3.png"))
]
LARGE_OBST = [
    pygame.image.load(os.path.join("assets", "Cactus", "LargeCactus1.png")),
    pygame.image.load(os.path.join("assets", "Cactus", "LargeCactus2.png")),
    pygame.image.load(os.path.join("assets", "Cactus", "LargeCactus3.png"))
]
FONT = pygame.font.Font("freesansbold.ttf", 20)

def distance(pos_a, pos_b):
    dx = pos_a[0]-pos_b[0]
    dy = pos_a[1]-pos_b[1]
    return math.sqrt(dx**2+dy**2)

def eval_genomes(src_genomes, config):
    clock = pygame.time.Clock()
    global points, game_speed, bg_pos, dinos, obstacles, genomes, nets, max_score
    points = 0
    game_speed = 20
    bg_pos = {"x": 0, "y":380}

    dinos = []
    obstacles = []
    # \NEAT
    genomes = []
    nets = []
    for genome_id, genome in src_genomes:
        dinos.append(Dino())
        genomes.append(genome)
        net = neat.nn.FeedForwardNetwork.create(genome, config)
        nets.append(net)
        genome.fitness = 0
    # /NEAT
    print(genomes)

    def score():
        global points, game_speed, max_score
        points += 1
        if max_score < points:
            max_score = points
        if points % 50 == 0:
            game_speed += 1
        text = FONT.render("Points: "+str(points), True, (0, 0, 0))
        textH = FONT.render("Highest score: "+str(max_score), True, (0, 0, 0))
        SCREEN.blit(text, (850, 50))
        SCREEN.blit(textH, (850, 70))

    def background():
        global bg_pos
        img_width = BG.get_width();
        SCREEN.blit(BG, (bg_pos["x"], bg_pos["y"]))
        SCREEN.blit(BG, (img_width + bg_pos["x"], bg_pos["y"]))
        if bg_pos["x"] <= -img_width:
            bg_pos["x"] = 0
        bg_pos["x"] -= game_speed

    def statistics():
        global dinos, game_speed, genomes
        text_dinos = FONT.render("Dinosaurs alive: "+str(len(dinos)), True, (0,0,0))
        text_gen = FONT.render("Generation: "+str(population.generation), True, (0,0,0))
        text_speed = FONT.render("Game speed: "+str(game_speed), True, (0,0,0))
        SCREEN.blit(text_dinos, (50, 450))
        SCREEN.blit(text_gen, (50, 470))
        SCREEN.blit(text_speed, (50, 490))

    run = True
    while(run):
        #before code
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
        SCREEN.fill((255,255,255))

        #end if all dinos dead
        if len(dinos) == 0:
            run = False
            break
        #generate random obstacles
        if len(obstacles) == 0:
            rand_int = random.randint(0,1)
            if rand_int:
                obstacles.append(SmallObstacle(SMALL_OBST, random.randint(0,2), SCREEN_WIDTH, game_speed))
            else:
                obstacles.append(LargeObstacle(LARGE_OBST, random.randint(0,2), SCREEN_WIDTH, game_speed))

        #update obstacles
        for obstacle in obstacles:
            obstacle.draw(SCREEN)
            obstacle.update(obstacles)
            #kill dino on collision
            for ix, dino in enumerate(dinos):
                if dino.rect.colliderect(obstacle.rect):
                    genomes[ix].fitness -= 1
                    dinos.remove(dino)
                    genomes.pop(ix)
                    nets.pop(ix)

        #update dinos
        for dino in dinos:
            dino.update()
            dino.draw(SCREEN, obstacles)


        #read input
        #user_input = pygame.key.get_pressed()
        #if user_input[pygame.K_SPACE]:
            #for dino in dinos:
                #dino.start_jump()
        for ix, dino in enumerate(dinos):
            output = nets[ix].activate((dino.rect.y,
                                       distance((dino.rect.x, dino.rect.y), obstacle.rect.midtop)))
            if output[0] > 0.5 and dino.rect.y == dino.POS["y"]:
                dino.start_jump()


        #after code
        score()
        background()
        statistics()
        clock.tick(30)
        pygame.display.update()


def run(config_path):
    global population
    config = neat.config.Config(
        neat.DefaultGenome,
        neat.DefaultReproduction,
        neat.DefaultSpeciesSet,
        neat.DefaultStagnation,
        config_path
    )
    population = neat.Population(config)
    population.run(eval_genomes, 50)

if __name__ == "__main__":
    local_dir = os.path.dirname(__file__)
    config_path = os.path.join(local_dir, "config.txt")
    run(config_path)
