from Obstacle import *

class LargeObstacle(Obstacle):
    def __init__(self, image, count, screen_width, game_speed):
        super().__init__(image, count, screen_width, game_speed)
        self.rect.y = 300
