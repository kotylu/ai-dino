class Obstacle:
    def __init__(self, image, count, screen_width, game_speed):
        self.img = image
        self.type = count
        self.rect = self.img[self.type].get_rect()
        self.rect.x = screen_width
        self.game_speed = game_speed

    def update(self, obstacles):
        self.rect.x -= self.game_speed
        if self.rect.x < -self.rect.width:
            obstacles.pop()

    def draw(self, screen):
        screen.blit(self.img[self.type], self.rect)
