import pygame, os, random


class Dino:
    RUNNING = [pygame.image.load(os.path.join("assets", "Dino", "DinoRun1.png")), pygame.image.load(os.path.join("assets", "Dino", "DinoRun2.png"))]
    JUMPING = pygame.image.load(os.path.join("assets", "Dino", "DinoJump.png"))
    JUMP_VEL = 8.5
    POS = {"x": 80, "y": 310}

    def __init__(self):
        self.pos = {"x": 80, "y": 310}
        self.jump_vel = self.JUMP_VEL
        self.img = self.RUNNING[0]
        self.step_ix = 0
        self.rect = pygame.Rect(self.POS["x"], self.POS["y"], self.img.get_width(), self.img.get_height())
        self.color = (random.randint(0,255), random.randint(0,255), random.randint(0,255))
        self.is_running = True
        self.is_jumping = False

    def update(self):
        if self.is_running:
            self.run()
        if self.is_jumping:
            self.jump()
        if self.step_ix > 9:
            self.step_ix = 0

    def start_jump(self):
        self.is_jumping = True
        self.is_running = False

    def jump(self):
        self.img = self.JUMPING
        print("-------")
        print(self.jump_vel)
        print(self.JUMP_VEL)
        print("-------")
        if self.is_jumping:
            self.rect.y -= self.jump_vel * 4
            self.jump_vel -= 0.8
        if self.jump_vel < -self.JUMP_VEL:
            self.is_jumping = False
            self.is_running = True
            self.jump_vel = self.JUMP_VEL

    def run(self):
        self.img = self.RUNNING[self.step_ix // 5]
        self.rect.x = self.POS["x"]
        self.rect.y = self.POS["y"]
        self.step_ix += 1

    def draw(self, screen, obstacles=[]):
        screen.blit(self.img, (self.rect.x, self.rect.y))
        pygame.draw.rect(screen, self.color, (self.rect.x, self.rect.y, self.rect.width, self.rect.height), 2)
        for obstacle in obstacles:
            pygame.draw.line(screen, self.color, (self.rect.x+54, self.rect.y+12), obstacle.rect.center, 2)
